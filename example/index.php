<?php
use Ardzo\ErrorProcessor\ErrorProcessor;

require 'MailSender.php';
require 'ErrorProcessor.php';
require 'lib_ouf.php';

$errP = new ErrorProcessor;
$errP->EP_to_addr = 'example@example.com';
$errP->EP_log_max_size = 2;

$errP->processError('Error message #1', 'm');
$errP->processError('Error message #2', 'l');
echo $errP->errWrite();
$errP->logSend();
$errP->logEvent('Event message #1', '0');
?>
