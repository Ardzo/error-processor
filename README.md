# Library for error processing

## Introduction
This is a class on PHP for processing of errors.  
Main features:  
- processes an errors  
- operates with a log of errors  
- sends a messages by e-mail

## Requirements
* PHP 4.1.0+
* Windows or Unix
* class "MailSender"
* library "Often Used Functions" V1.14 (http://ardzo.com/ru/soft_various.php)

## Using
Copy `"ErrorProcessor.php"` from `"src"` and required libraries onto server
and use it in your script.  
Or use Composer - class will loads automatically.  
See example code in folder `"example"`.
