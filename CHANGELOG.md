# Version history

## **2.7.2** 2018-10-18
* Requires library Often Used Functions V1.14

## **2.7.1** 2018-10-12
* Class and functions is renamed
* Namespace is added
* Other minor changes

## **2.7** 2010-10-03
* Log of errors now clears by size instead of a date.
* Log of events now also clears by size.
* Function `"log_send()"` can send both types of logs: errors and events.
* Function `"err_log_send()"` renamed to `"log_send()"`.
* Variable `"EP_log_period"` renamed to `"EP_log_max_size"`.
* Format of e-mail letter with log is little changed.

## **2.6** 2009-05-09
* Fixed error with new lines (\n, \r) in two functions

## **2.5** 2008-02-22
* Server variables is accessed with superglobals now
	(can work with "register_globals = off")

## **2.4** 2007-02-06
* Logging now writes IP-address

## **2.3** 2006-08-08
* Function `"send_mail()"` split to "prepare_letter()", "send_letter()"

## **2.2** 2005-07-13
* Added function for log an event

## **2.1** 2004-07-29
* Error log now really sends by E-mail on outflow of specified term

## **2.0** 2004-07-20
* Messages now not divides on modules. It now not have codes.
	Library operates straight with messages of errors.

## **1.0.1** 2004-05-01
* "chmod" now calls as "@chmod"

## **1.0** 2004-04-01
* This is first release
